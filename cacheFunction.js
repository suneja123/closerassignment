function cacheFunction(cb) {

    let cache = [];
    function invoke(arg)
    {
        if(cache.includes(arg))
        {
            console.log("you have been already come");
            return cache[arg];
        }
        else{
            cache.push(arg);
            cb(arg);
        }

    }
    return{
         invoke:invoke
    }
}
module.exports = { cacheFunction };
