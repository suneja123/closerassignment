function counterFactory() {
    var count = 0;
  
    function increment()
    {
        return ++count;
    }
    function decrement()
    {
        return --count;
    }
    

    return{
        increment,
        decrement
    }
}
module.exports =  { counterFactory };
