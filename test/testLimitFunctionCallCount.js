const countFunctionCall = require("./../limitFunctionCallCount");


const cb = () => console.log ("function has invoked");

const callInvoke = countFunctionCall.limitFunctionCallCount(cb,3);

callInvoke.invoke();
callInvoke.invoke();
callInvoke.invoke();
callInvoke.invoke();

